===============================================
Mandaye - modular reverse proxy to authenticate
===============================================

Mandaye is a modular rewriting reverse proxy. Its main use is to add external
authentication systems to legacy application, i.e. SSO.
It dispatches HTTP requests to your applications and allow you to transform and
filter the request and the response.
Mandaye allows to couple your authentication provider with incompatible web
applications.

Copying
-------

This software copyrighted by Entr'ouvert and is licensed under the GNU AGPL
version 3 for distribution.

Features
--------

 * Mappers / dispatching::
   With Mandaye you can define your own mapping files. This allows you to call your own filters
   on the right HTTP requests. See the config part for more details.
 * Filters::
   You can define your own filters with Mandaye. This filter have access to the WSGI environment and
   could modify the HTTP requests and / or responses.
 * Local authentification::
   Mandaye provide a sql local authentification but you can also implement your own
   local authentification.
 * Distant authentification::
   At the moment Mandaye only support SAML 2.0 with password replay but we will provide
   SAML 2.0, OAuth and CAS support.


Installation
============

Dependencies
------------

The dependencies are in define into requirements.txt

Quick installation
------------------

Install at least Python >=2.6 and pip in your system.
For example with Debian or a Debian based distribution::

    sudo apt-get install python python-pip

Then install virtualenv ::

   pip install virtualenv

Create your virtualenv activate it::

    virtualenv mandaye
    source mandaye/bin/activate
    pip install -U pip

Install mandaye::

    $ tar xfvz mandaye-VERSION.tar.gz
    $ cd mandaye-VERSION
    $ pip install -r requirements.txt
    $ python setup.py install

If you want to develop use this command line::

    $ python setup.py develop


Quick Start
-----------

First step is to create a mandaye project::

    $ mandaye-admin.py --newproject PROJECT_NAME
    $ cd PROJECT_NAME

Install your Mandaye project::

    $ python setup.py install
    $ ./manager.py --createdb

Launch mandaye server::

    $ ./server.py

Use your own Mandaye configuration file::

    $ cp PROJECT_NAME/default-config.ini PROJECT_NAME/config.ini
    $ ./server.py --config=config.ini

server.py use gunicorn and gunicorn options (please read http://gunicorn.org/configure.html)

You could also use gunicorn.conf.py-sample (in the mandaye files)::

 $ ./server.py --config=config.ini -c PATH_TO_THE_FILE/gunicorn.conf.py

or::

 $ ./server.py --config=config.ini -c PATH_TO_THE_FILE/gunicorn.conf.py -b 0.0.0.0:4242

Upgrade Mandaye
---------------

You need to upgrade the update the repository and upgrade database::

 ~/new_mandaye $ python setup.py install
 ~/new_mandaye $ alembic upgrade head

Configuration
=============
TODO

function mandaye_disassociate_logout(url, account, id, next_url)
{
  var r = confirm("Etes-vous sûr de vouloir désassocier le compte " + account + " ?");

  if (r == true)
  {
    if (url.search(/\?/) > 0)
      url += "&id=" + id;
    else
      url += "?id=" + id;
    if (next_url)
      url += "&next_url=" + encodeURIComponent(next_url);
    window.location = url;
  }
}

(function( $ ) {
    $(function() {
          $.get("/mandaye/toolbar", function(data) {
            $("body").prepend(data);
          });
    });
})(mjQuery);

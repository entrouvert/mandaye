'''
Default urllib2 handlers for Mandaye
'''

import urllib2
import urllib

class MandayeRedirectHandler(urllib2.HTTPRedirectHandler):

    def http_error_301(self, req, fp, code, msg, headers):
        """ Disable the auto redirect """
        infourl = urllib.addinfourl(fp, headers, req.get_full_url())
        infourl.code = code
        infourl.msg = msg
        return infourl

    def http_error_302(self, req, fp, code, msg, headers):
        """ Disable the auto redirect """
        infourl = urllib.addinfourl(fp, headers, req.get_full_url())
        infourl.code = code
        infourl.msg = msg
        return infourl

class MandayeErrorHandler(urllib2.HTTPDefaultErrorHandler):
    def http_error_default(self, req, fp, code, msg, headers):
        result = urllib2.HTTPError(req.get_full_url(), code, msg, headers, fp)
        result.status = code
        return result



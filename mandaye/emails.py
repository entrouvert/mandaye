import smtplib

from email.mime.text import MIMEText

from mandaye import config
from mandaye.log import logger

class Email:

    def __init__(self):
        """
        """
        self.config_ok = True
        if not config.smtp_host or not config.smtp_port or \
                not config.email_from or not config.email_to:
            logger.warning('[Config] Bad email notification configuration')
            self.config_ok = False

    def sent(self, subject, msg, type='plain'):
        """ sent an email
        msg : email text body
        subject; email subject
        """
        if not self.config_ok:
            logger.warning('[Config] Send email %s failed: bad configuration' % msg)
        # build email
        msg = MIMEText(msg, type)
        msg['Subject'] = config.email_prefix + subject
        msg['From'] = config.email_from
        msg['To'] = ', '.join(config.email_to)
        # sent email
        s = smtplib.SMTP(config.smtp_host, config.smtp_port)
        s.sendmail(config.email_from, config.email_to, msg.as_string())
        s.quit()



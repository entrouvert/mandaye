"""
Global Mandaye exception and warning classes.
"""

class ImproperlyConfigured(Exception):
    "Mandaye is somehow improperly configured"
    pass

class MandayeException(Exception):
    "Mandaye generic exception"
    pass

class MandayeSamlException(Exception):
    "Mandaye SAML 2 exception"
    pass

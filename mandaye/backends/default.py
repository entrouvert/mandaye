
from datetime import datetime
from importlib import import_module

from mandaye import config
from mandaye.exceptions import ImproperlyConfigured

def import_backend(path):
    try:
        mod = import_module(path)
    except ImportError, e:
        raise ImproperlyConfigured('Error importing backend %s: "%s"' % (path, e))
    return mod

storage_conn = None
if config.storage_backend == "mandaye.backends.sql":
    from sqlalchemy import create_engine
    from sqlalchemy.orm import sessionmaker, scoped_session
    if not "sqlite" in config.db_url:
       storage_conn = scoped_session(
                sessionmaker(
                    bind=create_engine(config.db_url, pool_size=16,
                        pool_recycle=1800)
                    )
                )
    else:
        storage_conn = scoped_session(
                sessionmaker(
                    bind=create_engine(config.db_url)
                    )
                )
elif config.storage_backend == "mandaye.backends.ldap_back":
    import ldap
    storage_conn = ldap.initialize(config.ldap_url)
    storage_conn.protocol_version = ldap.VERSION3
    storage_conn.simple_bind(config.ldap_bind_dn, config.ldap_bind_password)

backend = import_backend(config.storage_backend)
Association = backend.Association

class AssociationExample(object):
    """
    association dictionary return by the following methods:
    {
        'id': '', # identifier of your association (must be unique)
        'sp_name': '', # name of the service provider (defined in the mappers)
        'sp_login': '', # login on the service provider
        'sp_post_values': '', # the post values for sp login form
        'idp_unique_id:': '', # the unique identifier of the identity provider (ex.: a saml NameID)
        'idp_name':  '', # identity provide name
        'last_connection':  datetime.datetime, # last connection with this association
        'creation_date':  datetime.datetime, # creation date of this association
    }
    """

    @staticmethod
    def get(sp_name, idp_unique_id, idp_name='default'):
        """ return a list of dict with associations matching all of this options """
        pass

    @staticmethod
    def get_by_id(asso_id):
        """ return a dict of the association with the id or None if it doesn't exist """
        pass

    @staticmethod
    def has_id(asso_id):
        """ check the given user is present """
        pass

    @staticmethod
    def update_or_create(sp_name, sp_login, sp_post_values, idp_unique_id,
            idp_name='default', creation_date=None, last_connection_date=None):
        """ update or create an associtaion which match the following values
        creation_date and last_connection_date: by default datetime.utcnow()
        return the association id
        """
        pass

    @staticmethod
    def delete(asso_id):
        """ delete the association which has the following asso_id """
        pass

    @staticmethod
    def get_last_connected(sp_name, idp_unique_id, idp_name='default'):
        """ get the last connecting association which match the parameters
        return a dict of the association
        """
        pass

    @staticmethod
    def update_last_connection(asso_id):
        """ update the association last connection time with the current time
        return a dict of the association
        """
        pass

    @staticmethod
    def has_sp_login(sp_login, sp_name):
        """ Test if a service provider login is present on the databases
        """
        pass

import os

from ConfigParser import SafeConfigParser
from importlib import import_module
from mandaye.exceptions import ImproperlyConfigured

BASE_DIR = os.path.dirname(os.path.abspath(__file__))

# get configuration files from :
# 1. default-settings.ini from source code
# 2. from MANDAYE_CONFIG_FILE environ variable
SETTINGS_INI = [os.path.join(BASE_DIR, 'default-config.ini')]
if 'MANDAYE_CONFIG_FILES' in os.environ:
    print 'Loading setting files %r' % os.environ['MANDAYE_CONFIG_FILES']
    SETTINGS_INI += os.environ['MANDAYE_CONFIG_FILES'].split(' ')
config = SafeConfigParser()
config.read(SETTINGS_INI)


def section2dict(section):
    res = dict()
    if config.has_section(section):
        for opt in config.options(section):
            res[opt] = config.get(section, opt)
    return res

## SQL Backend config
# Database configuration
# dialect+driver://username:password@host:port/database
db_url = config.get('database', 'url')

## LDAP Backend config
ldap_url = config.get('ldap', 'url')
ldap_bind_dn = config.get('ldap', 'bind_dn')
ldap_bind_password = config.get('ldap', 'bind_password')
ldap_base_dn = config.get('ldap', 'base_dn')

debug = config.getboolean('debug', 'debug')

# Log configuration
logger_name = config.get('debug', 'logger_name')
LOGGING = {
        'version': 1,
        'disable_existing_loggers': True,

        'formatters': {
            'console': {
                'format': '%(asctime)s %(levelname)s %(message)s',
                'datefmt': '%H:%M:%S',
                },
            'syslog': {
                'format': 'mandaye(pid=%(process)d) %(name)s %(levelname)s %(uuid)s %(message)s',
                'datefmt': '%Y-%m-%d %H:%M:%S'
                }
            },
        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'console'
                },
            'syslog': {
                'level': 'DEBUG',
                'class': 'entrouvert.logging.handlers.SysLogHandler',
                'formatter': 'syslog',
                'address': '/dev/log'
                },
            },
        'loggers': {
            '': {
                'handlers': ['console'],
                'level': 'INFO',
                'propagate': False,
                },
            'mandaye': {
                'handlers': ['console', 'syslog'],
                'level': 'INFO',
                'propagate': False,
                },
            logger_name: {
                'handlers': ['console', 'syslog'],
                'level': 'INFO',
                'propagate': False,
                },
            },
        }

if config.getboolean('debug', 'log_debug'):
    LOGGING['loggers']['']['level'] = 'DEBUG'
    LOGGING['loggers']['mandaye']['level'] = 'DEBUG'
    LOGGING['loggers'][logger_name]['level'] = 'DEBUG'

## PATH
module_name = config.get('dirs', 'module_name')
module_directory = None
if module_name:
    try:
        mandaye_project = import_module(module_name)
        module_directory = os.path.dirname(mandaye_project.__file__)
    except ImportError, e:
        raise ImproperlyConfigured('Error importing %r from configuration', module_name)
# Configuration directory
config_root = config.get('dirs', 'config_root')
mandaye_template_directory = os.path.join(BASE_DIR, 'templates')
module_template_directory = None
if module_directory:
    module_template_directory = os.path.join(module_directory, 'templates')
templates_directories = []
# Templates directories
if config.get('dirs', 'templates_directories'):
    templates_directories = config.get('dirs', 'templates_directories').split(' ')
if module_template_directory:
    templates_directories.append(module_template_directory)
templates_directories.append(mandaye_template_directory)
# Static url
static_url = config.get('dirs', 'static_url')
# Static folder
if config.get('dirs', 'static_root'):
    static_root = config.get('dirs', 'static_root')
else:
    static_root = os.path.join(module_directory, 'static')
# Data dir
data_dir = config.get('dirs', 'data_dir')
skel_root = os.path.join(BASE_DIR, 'skel')

# template vars
template_vars = section2dict('template_vars')

# Supported authentification
authentifications = section2dict('authentifications')

# sp mappers
mappers = section2dict('mappers')

# Raven Sentry configuration
raven_dsn = config.get('debug', 'sentry_dsn')

# Email notification configuration
email_notification = config.getboolean('email', 'notification')
email_prefix = config.get('email', 'prefix')
smtp_host = config.get('email', 'smtp_host')
smtp_port = config.getint('email', 'smtp_port')
email_from = config.get('email', 'from')
email_to = config.get('email', 'to').split()

# Alembic configuration
alembic_cfg = os.path.join(BASE_DIR, 'alembic.ini')
alembic_script_path = os.path.join(BASE_DIR, 'alembic')

# Use long traceback with xtraceback
use_long_trace = config.getboolean('debug', 'use_long_trace')
# Ask Mandaye to auto decompress a response message
# Decompress response only if you load a filter
auto_decompress = config.getboolean('mandaye', 'auto_decompress')
# Ask mandaye to add a toolbar with Mandaye's links
mandaye_toolbar = config.getboolean('mandaye', 'toolbar')
mandaye_offline_toolbar = config.getboolean('mandaye', 'offline_toolbar')
# Authentic 2 auto connection
a2_auto_connection = config.getboolean('mandaye', 'a2_auto_connection')

# Choose storage (sql or ldap)
if config.get('mandaye', 'storage_backend') == 'sql':
    storage_backend = "mandaye.backends.sql"
elif config.get('mandaye', 'storage_backend') == 'ldap':
    storage_backend = "mandaye.backends.ldap_back"
else:
    ImproperlyConfigured('Storage backend must be sql or ldap')

# Encrypt service provider passwords with a secret
# You should install pycypto to use this feature
encrypt_sp_password = config.getboolean('mandaye', 'encrypt_sp_password')
# Must be a 15, 24, or 32 bytes long
encrypt_secret = config.get('mandaye', 'encrypt_secret')

session_type = config.get('session', 'type')
if session_type not in ('file', 'dbm', 'memory', 'memcached'):
    raise ImproperlyConfigured('Sesssion type %r not supported' % session_type)
if session_type == 'memcached':
    session_type = 'ext:memcached'

# Beaker session configuration
session_opts = {
    'session.type': session_type,
    'session.url': config.get('session', 'url'),
    'session.cookie_expires': config.getboolean('session', 'cookie_expires'),
    'session.timeout': config.getint('session', 'timeout'),
    'session.data_dir': config.get('session', 'data_dir')
}


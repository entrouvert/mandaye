
"""
You need to defined 3 variables :

* form_values (defined the login form values):
form_values = {{
    'login_url': '/login',
    'post_url': '/login',
    'form_attrs': {{ 'name': 'form40', }},
    'username_field': 'user',
    'password_field': 'pass',
    'post_fields': ['birthdate', 'card_number']
}}
login_url, form_attrs, post_fields and username_field are obligatory
* urls (a dictionnary with urls) :
 urls = {{
       'associate_url': '/mandaye/associate',
       'connection_url': '/mandaye/sso',
       'login_url': '/mandaye/login'
       }}
* mapping
"""

from mandaye.auth.saml2 import END_POINTS_PATH
from {project_name}.filters.example import ReplayFilter

form_values = {{
        'login_url': '/compte/connexion',
        'form_attrs': {{ 'id': 'new_account' }},
        'post_fields': ['account[login]', 'account[password]'],
        'username_field': 'account[login]',
        'password_field': 'account[password]',
}}

urls = {{
    'associate_url': '/mandaye/associate',
    'connection_url': '/mandaye/sso',
    'disassociate_url': '/mandaye/disassociate',
    'login_url': '/mandaye/login'
}}

mapping = [
        {{
            'path': r'/mandaye/login$',
            'method': 'GET',
            'response': {{
                'auth': 'login',
                'values': {{'condition': 'response.code==302'}},
                }},
            }},  
        {{   
            'path': r'/mandaye/sso$',
            'method': 'GET',
            'response': {{'auth': 'sso',}}
            }},  
        {{   
            'path': r'/mandaye/slo$',
            'method': 'GET',
            'response': {{'auth': 'slo',}}
            }}, 
        {{
            'path': r'/mandaye/associate$',
            'method': 'GET',
            'on_response': [{{
                'filter': ReplayFilter.associate,
                'values': {{
                    'action': urls['associate_url'],
                    'template': 'associate.html',
                    'sp_name': 'Linux FR',
                    'login_name': form_values['username_field'],
                    'password_name': form_values['password_field'],
                    }},
                }},]
            }},
        {{
            'path':  r'/mandaye/associate$',
            'method': 'POST',
            'response': {{
                'auth': 'associate_submit',
                'values': {{'condition': "response.code==302"}}
                }},
            }},
        {{
            'path': r'%s$' % END_POINTS_PATH['single_sign_on_post'],
            'method': 'POST',
            'response': {{'auth': 'single_sign_on_post'}}
            }},
        {{
            'path': r'%s$' % END_POINTS_PATH['single_logout'],
            'method': 'GET',
            'response': {{'auth': 'single_logout',}}
            }},
        {{
            'path': r'%s$' % END_POINTS_PATH['single_logout_return'],
            'method': 'GET',
            'response': {{'auth': 'single_logout_return',}}
            }},
        ]


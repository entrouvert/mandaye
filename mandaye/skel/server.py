#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Script to launch mandaye with gunicorn server
"""

import os
import sys

from gunicorn.app.wsgiapp import WSGIApplication

class MandayeWSGIApplication(WSGIApplication):

    def init(self, parser, opts, args):
        self.cfg.set("default_proc_name", "{project_name}.wsgi:application_dev")
        self.app_uri = "{project_name}.wsgi:application_dev"

def main():
    """ The ``gunicorn`` command line runner for launcing Gunicorn with
    generic WSGI applications.
    """
    config_file = None
    config_arg_pos = None
    for i, arg in enumerate(sys.argv[1:]):
        if arg.startswith('--config='):
            config_file = arg.split('=')[1]
            config_arg_pos = i
    if config_file:
        os.environ['MANDAYE_CONFIG_FILES'] = config_file
    if config_arg_pos is not None:
        del sys.argv[config_arg_pos + 1]
    MandayeWSGIApplication("%(prog)s [OPTIONS]").run()

if __name__ == "__main__":
    main()


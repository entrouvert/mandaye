#! /usr/bin/env python

'''
   Setup script for {project_name} RP
'''

import os
import subprocess

from setuptools import setup, find_packages
from sys import version

import {project_name}

install_requires=[
        'gunicorn>=0.17',
        'mandaye>=0.8.0',
        'whitenoise>=1.0'
]

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        return version.replace('-','.')
    return {project_name}.__version__

setup(name="{project_name}",
      version=get_version(),
      license="AGPLv3 or later",
      description="{project_name} rp is a Mandaye project, modular reverse proxy to authenticate",
      url="http://dev.entrouvert.org/projects/reverse-proxy/",
      author="Author",
      author_email="author@example.com",
      maintainer="Maintainer",
      maintainer_email="maintainer@exmaple.com",
      scripts=['{project_name}_manager', '{project_name}_server'],
      packages=find_packages(),
      include_package_data=True,
      install_requires=install_requires
)


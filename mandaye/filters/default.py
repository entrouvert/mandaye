
import re
from mandaye import config
from mandaye.backends.default import Association
from mandaye.log import logger
from mandaye.response import template_response

class MandayeFilter(object):
    """ Default Mandaye filter
    This class allows to filter the request (cookies, headers and the message-body)
    and the response (cookies, headers and message-body)
    """

    @staticmethod
    def on_request(env, values, request):
        target = env['target']
        referer = request.headers.getheader('referer')
        if referer:
            referer = referer.replace(env["HTTP_HOST"], target.hostname)
            request.headers.addheader("Referer", referer)
        origin = request.headers.getheader('origin')
        if origin:
            origin = origin.replace(env["HTTP_HOST"], target.hostname)
            request.headers.addheader("Origin", origin)
        # Add forwarded header like Apache
        request.headers.addheader('X-Forwarded-For', env['REMOTE_ADDR'])
        request.headers.addheader('X-Forwarded-Host', env['HTTP_HOST'])

    @staticmethod
    def on_response(env, values, request, response):
        target = env['target']
        # Modify cookies
        src_hostname = re.sub(":\d*$", "", env["HTTP_HOST"])
        for cookie in response.cookies.values():
            if cookie.has_key('domain'):
                cookie['domain'] = re.sub('^\.*%s$' % (target.hostname),
                        src_hostname, cookie['domain'])
        # Modify headers
        if response.headers.has_key('location'):
            response.headers.addheader('location',
                    re.sub(r'^http(s*)://%s' % env["target"].hostname,
                        r"%s://%s" % (env["mandaye.scheme"], env["HTTP_HOST"]),
                        response.headers.getheader('location'),
                        flags=re.IGNORECASE)
                    )
        blacklist = ['transfer-encoding', 'content-length']
        for name in blacklist:
            if response.headers.has_key(name):
                del response.headers[name]
        return response

    @staticmethod
    def fix_response_abs_url(env, values, request, response):
        if response.msg:
            response.msg = re.sub(r'%s[:\d]*' % env["target"].geturl(),
                    r"%s://%s" % (env["mandaye.scheme"], env["HTTP_HOST"]),
                    response.msg, flags=re.IGNORECASE)
            if env["mandaye.scheme"] == 'https':
                response.msg = response.msg.replace('http://' + env["HTTP_HOST"],
                        '%s://%s' % (env["mandaye.scheme"], env["HTTP_HOST"]))
        return response

    @staticmethod
    def add_js_header(env, values, request, response):
        if response.msg:
            if not "mjQuery = jQuery.noConflict" in response.msg:
                response.msg = re.sub(
                        r'<head(.*?)>',
                        r"""<head\1>
                <script src="%s/js/jquery-1.11.1.min.js" type="text/javascript"></script>
                <script>
                mjQuery = jQuery.noConflict( true );
                </script>""" % (config.static_url),
                response.msg, 1, re.IGNORECASE)
            scripts_html = ""
            for script in values["scripts"]:
                scripts_html += '<script src="%s/%s" charset="UTF-8" type="text/javascript"></script>' % \
                        (config.static_url, script)
            response.msg = re.sub(
                    '</head>',
                    '%s</head>' % scripts_html,
                    response.msg, 1, re.IGNORECASE)
        return response

    @staticmethod
    def addtoolbar(env, values, request, response):
        session = env['beaker.session']
        if config.mandaye_offline_toolbar or \
                env['beaker.session'].has_key('unique_id'):
            response.msg = re.sub(
                    '<head.*?>',
                """<head>
                <script src="%s/js/jquery-1.11.1.min.js" type="text/javascript"></script>
                <script>
                mjQuery = jQuery.noConflict( true );
                </script>""" % (config.static_url),
                response.msg, 1)
            response.msg = response.msg.replace(
                '</head>',
                """<script src="%s/js/toolbar.js" charset="UTF-8" type="text/javascript"></script>
                 <link rel="stylesheet" type="text/css" href="%s/css/toolbar.css">
                 </head>""" % (config.static_url, config.static_url))
        return response

    @staticmethod
    def toolbar(env, values, request, response):
        values['urls'] =  env['urls']
        values['site_name'] =  env["mandaye.config"]["site_name"]
        values['is_login'] = False
        values['is_user_locally_logged_in'] = None
        values['is_user_associated'] = env['beaker.session'].get('is_associated')
        if hasattr(env['mandaye.mapper'], 'is_user_locally_logged_in'):
            values['is_user_locally_logged_in'] = env['mandaye.mapper'].\
                    is_user_locally_logged_in(env, request, response)
        current_account = None
        if env['beaker.session'].get('unique_id'):
            values['is_login'] = True
            site_name = env["mandaye.config"]["site_name"]
            if env['beaker.session'].get(site_name):
                logger.debug('toolbar there is one : %r' %\
                        env['beaker.session'].get(site_name))
                current_account = Association.get_by_id(env['beaker.session'].get(site_name))
            else:
                logger.debug('toolbar: no account')
        values['account'] = current_account
        return template_response("toolbar.html", values)

    @staticmethod
    def store_request_content_buffer(env, values, request):
        """ on_request filter which allows to store the content of a post in request object
        """
        if request.msg and not isinstance(request.msg, basestring):
            request.msg = request.msg.read()

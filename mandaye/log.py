
import logging
from logging.config import dictConfig

from mandaye import config

class UuidFilter(logging.Filter):
    uuid = ""

    def filter(self, record):
        record.uuid = UuidFilter.uuid
        return True

dictConfig(config.LOGGING)
logger = logging.getLogger(__name__)
logger.addFilter(UuidFilter())


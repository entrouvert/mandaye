
from mandaye.filters.default import MandayeFilter

from mandaye import config

mapping = [
        {
            'path': r'/',
            'on_request': [
                {'filter': MandayeFilter.on_request},
                ],
            'on_response': [
                {'filter': MandayeFilter.on_response,},
                ]
            },
        ]

if config.mandaye_toolbar:
    mapping.extend([
        {
            'path': r'/',
            'on_response': [
                {
                    'filter': MandayeFilter.addtoolbar,
                    'content-types': ['text/html'],
                    }
                ]
            },
        {
            'path': r'/mandaye/toolbar',
            'response': {'filter': MandayeFilter.toolbar}
            },
        ])

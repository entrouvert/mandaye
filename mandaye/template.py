
from mako.lookup import TemplateLookup
from mako.template import Template

from mandaye import config

mylookup = TemplateLookup(directories=config.templates_directories,
        input_encoding='utf-8')

def serve_template(templatename, encoding='utf-8', **kwargs):
    """ serve a template
    """
    kwargs.update(config.template_vars)
    mytemplate = mylookup.get_template(templatename)
    return mytemplate.render_unicode(static_url=config.static_url,
            **kwargs).encode(encoding, 'replace')

def get_absolute_uri(env):
    return env['mandaye.scheme'] + '://' + env['HTTP_HOST'] + \
        env['PATH_INFO']

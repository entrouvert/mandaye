
import json
import pprint
import sys
import traceback
import xtraceback

from cgi import escape

from mandaye import config
from mandaye.log import logger
from mandaye.http import HTTPResponse, HTTPHeader
from mandaye.emails import Email

from mandaye.template import serve_template

email = None
if config.email_notification:
    email = Email()

def _get_html_error(tb_str, path, env=None):
    """ Return a full html error with exception and environment
    """
    msg = "<html><head><title>Mandaye traceback</title></head>"
    msg += "<h2>%s</h2><h3>Exception</h3><pre>%s</pre>" % (path, escape(tb_str))
    if env:
        env = pprint.pformat(env)
        msg += '<h3>Environment</h3><pre>%s</pre>' % escape(env)
    msg += '</html>'
    return msg

def _get_text_error(tbr_str, path, env=None):
    """ Return a full text error with exception and environment
    """
    error = "= Mandaye Traceback =\n\n"
    error += "Path: %s\n\n== Exception ==\n" % path
    error += tbr_str
    error += "\n== Environment ==\n%s" % env
    return error

def _get_traceback():
    """ Return a string wih the traceback
    """
    if config.use_long_trace:
        tb_str = str(xtraceback.XTraceback(*sys.exc_info(), color=False))
    else:
        tb_str = traceback.format_exc()
    return tb_str

def _302(location, cookies=None):
    headers = HTTPHeader({'location': [location],
        'Content-Type': ['text/html']})
    msg = "Reload the page to get source for: %s" % location
    logger.info('302 redirect to %s' % location)
    return HTTPResponse(302, 'Found', headers, msg, cookies)

def _401(msg):
    title='Unauthorized'
    logger.warning('401 %s' % msg)
    headers = HTTPHeader({'Content-Type': ['text/html']})
    return HTTPResponse(401, 'Unauthorized', headers,
            serve_template("response.html", title=title, body=msg))

def _404(path):
    logger.warning("404 %s not found" % path)
    title = "Not Found"
    body = "The requested URL %s was not found on this server." % path
    headers = HTTPHeader({'Content-Type': ['text/html']})
    return HTTPResponse(404, 'Not found', headers,
            serve_template("response.html", title=title, body=body))

def _500(path, msg, exception=None, env=None):
    """ Return a 500 error
    path: the path of this error
    exception: this exception instance if any """
    if exception:
        tb_str = _get_traceback()
        logger.exception("500 %s: %s, %s, env: %r" % (path, msg, tb_str, env))
        if email:
            try:
                email.sent('internal server error %r' % exception,
                        _get_text_error(tb_str, path, env=env))
            except Exception, e:
                logger.exception(e)
    else:
        logger.error("500 %s: %s, env: %r" % (path, msg, env))
    title = 'Internal Server Error'
    headers = HTTPHeader({'Content-Type': ['text/html']})
    if config.debug:
        if exception:
            msg += _get_html_error(tb_str, path, env=env)
        else:
            msg=msg
    else:
        msg = "The Mandaye server encountered an internal error or misconfiguration \
and was unable to complete your request. Please contact the server \
administrator and inform them of the time the error occurred."
    msg = msg.encode('utf8')
    return HTTPResponse(500, 'Internal Server Error', headers,
            serve_template("response.html", title=title, body=msg))

def _502(path, target_hostname, exception=None):
    """ Return a 502 error
    path: the path of this error
    exception: this exception instance if any """
    logger.error("502 error while reading %s" % path)
    title = 'Bad Gateway'
    headers = HTTPHeader({'Content-Type': ['text/html']})
    msg = "The Mandaye server can't join the target server or misconfiguration \
and was unable to complete your request. Please contact the server \
administrator and inform them of the time the error occurred."
    if config.debug and exception:
        msg += "<h2>Target hostname: %s</h2>" % target_hostname
        msg += _get_html_error(_get_traceback(), path)
    return HTTPResponse(502, 'Bad Gateway', headers,
            serve_template("response.html", title=title, body=msg))

def template_response(templatename, values):
    headers = HTTPHeader({'Content-Type': ['text/html'], 'Cache-Control': ['no-cache']})
    msg = serve_template(templatename, **values)
    return HTTPResponse(200, 'OK', headers, msg)

def json_response(msg):
    headers = HTTPHeader({'Content-Type': ['application/json'], 'Cache-Control': ['no-cache']})
    msg = json.dumps(msg)
    return HTTPResponse(200, 'OK', headers, msg)

def json_bad_request(msg):
    headers = HTTPHeader({'Content-Type': ['application/json'],
        'Cache-Control': ['no-cache']})
    msg = json.dumps(msg)
    return HTTPResponse(400, 'Bad Request', headers, msg)

def json_error(msg):
    headers = HTTPHeader({'Content-Type': ['application/json'],
        'Cache-Control': ['no-cache']})
    msg = json.dumps(msg)
    return HTTPResponse(500, 'Internal Server Error', headers, msg)


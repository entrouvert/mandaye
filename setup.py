#! /usr/bin/env python

'''
   Setup script for Mandaye
'''

import os
import subprocess

from setuptools import setup, find_packages
from sys import version

install_requires=[
          'alembic>=0.4',
          'beaker>=1.6',
          'mako>=0.4',
          'sqlalchemy>=0.7.3',
          'lxml>=2.0',
          'python-entrouvert',
          'xtraceback>=0.3',
]

if version < '2.7':
    install_requires.append('importlib')

def get_version():
    if os.path.exists('VERSION'):
        version_file = open('VERSION', 'r')
        version = version_file.read()
        version_file.close()
        return version
    if os.path.exists('.git'):
        p = subprocess.Popen(['git','describe','--match=v*'],
                stdout=subprocess.PIPE)
        result = p.communicate()[0]
        version = result.split()[0][1:]
        return version.replace('-','.')
    import mandaye
    return mandaye.__version__

setup(name="mandaye",
      version=get_version(),
      license="AGPLv3 or later",
      description="Mandaye, modular reverse proxy to authenticate",
      url="http://dev.entrouvert.org/projects/reverse-proxy/",
      author="Entr'ouvert",
      author_email="info@entrouvert.org",
      maintainer="Jerome Schneider",
      maintainer_email="jschneider@entrouvert.com",
      scripts=['scripts/mandaye-admin.py'],
      include_package_data = True,
      packages=find_packages(),
      install_requires=install_requires,
)


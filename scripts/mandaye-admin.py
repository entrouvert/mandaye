#! /usr/bin/python
# -*- coding: utf-8 -*-

""" Script to create mandaye projects
"""

import os
import re
import shutil
import sys

from optparse import OptionParser

from mandaye import config
from mandaye.log import logger

def get_cmd_options():
    usage = "usage: %prog --newproject"
    parser = OptionParser(usage=usage)
    parser.add_option("-n", "--newproject",
            dest="project_name",
            metavar="PROJECT_NAME",
            help="PROJECT_NAME: the name of teh new mandaye's project"
            )
    (options, args) = parser.parse_args()
    if not options.project_name:
        parser.error("You must set --newproject option")
    if options.project_name:
        if not re.search(r'^[_a-zA-Z]\w*$', options.project_name):
            parser.error("project_name %s is not a valid name."
                    "Please use use only numbers, letters and underscores.")
    return options

def main():
    options = get_cmd_options()
    if options.project_name:
        project_name = options.project_name
        module = os.path.join(project_name,
                project_name)
        modue_example = os.path.join(project_name,
                "example.module")
        skel = config.skel_root
        logger.info("Creating project %s ..." % project_name)
        if os.path.exists(project_name):
            print "%s folder already exist" % project_name
            sys.exit(1)
        shutil.copytree(skel, project_name)
        for root, dirs, files in os.walk(project_name):
            if not "templates" in root and not "static" in root:
                for filename in files:
                    file_path = os.path.join(root, filename)
                    with open(file_path, "r") as f:
                        content = f.read()
                    with open(file_path, "w") as f:
                        f.write(content.format(project_name=project_name))
        shutil.move(modue_example, module)

if __name__ == "__main__":
    main()

